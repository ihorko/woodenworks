<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Add relations between tables !!!

        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_id');
            $table->string('name', 255);
            $table->text('description')->nullable();
            // probably should be added an option for image
            $table->timestamps();
        });

        /*
        probably should be added categories_i18n like
        Schema::create('categories_i18n', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id');
            $table->string('lang', 255);
            $table->string('name', 255);
            $table->text('description');
        });
        and removed name and description columns from categories table
        as well as for products and properties
        later these translations might be cached with memcache or something
         */

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->text('description');
            // price as integer, should be transformed after retrieving from DB
            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('category_id');
            $table->boolean('in_stock');
            $table->timestamps();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('type', 255); //?
            $table->timestamps();
        });

        Schema::create('property_values', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->string('value', 255);
            $table->text('meta');
        });
        // properties need more effort to finalize
        // such things as media, pictures and videos, colors, sizes,
        // probably property options table should be added (like values for colors or sizes, or CPU's...)
        // and one table for relations between products and property options (maybe make media as separate table)

        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            // probably requires separate table with order steps
            $table->enum('status', ['completed', 'delivering', 'processing', /*any other steps*/]);
            // also requires delivery information, payment information, discounts, promo, etc
            $table->unsignedBigInteger('total')->comment('Total amount to be paid.');
            $table->timestamps();
        });

        Schema::create('ordered', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');
            $table->integer('quantity', unsigned: true);
            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('discount');
            $table->timestamps();
        });

        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->string('code', 255);
            $table->float('amount', places: 2);
            $table->text('description');
            $table->dateTime('ends_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('products');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('property_values');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('ordered');
        Schema::dropIfExists('discounts');

    }
}
