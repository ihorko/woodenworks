<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        return view('admin.category.index', ['categories' => $categories]);
    }

    public function show(Request $request)
    {
        return view('admin.category.show');
    }

    public function create(Request $request)
    {
        $categories = Category::all();
        return view('admin.category.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'parent' => ['exclude_if:parent,0', 'required', 'exists:App\Models\Category,id'],
            'description' => ['string'],
        ]);

        $category = Category::create([
            'name' => $request->input('name'),
            'parent_id' => $request->input('parent'),
            'description' => $request->input('description')
        ]);

        return redirect(route('admin.categories.index'));
    }

    public function destroy(Request $request, Category $category)
    {
        $category->delete();
        return redirect(route('admin.categories.index'));
    }
}
