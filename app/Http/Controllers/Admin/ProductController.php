<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::all();
        return view('admin.product.index', ['products' => $products]);
    }

    public function create(Request $request)
    {
        return view('admin.product.create');
    }
}
