<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix' => '/admin',
    'as' => 'admin.',
    'namespace' => 'App\Http\Controllers\Admin',
    'middleware' => [],
], function() {
    Route::get('/', function() {
        return view('admin.index');
    })->name('index');
    Route::get('/orders', function() {
        return view('admin.dashboard');
    })->name('orders');
    Route::resource('products', ProductController::class)->only([
        'index', 'show', 'create'
    ])->names([
        'index' => 'products',
        'create' => 'products.create'
    ]);
    Route::resource('categories', CategoryController::class);

    Route::get('/files', function(){
        //FileController::class

        return view('admin.file.index');
    })->name('files');

    Route::get('/users', function() {
        return view('admin.dashboard');
    })->name('users');
});
