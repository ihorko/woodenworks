@extends('admin.layout')

@section('content')

    <div class="container px-6 mx-auto grid">
        <h2
            class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200"
        >
            Add category
            {{-- probably create a card with an option to enter category's data --}}
        </h2>

        <form action="{{ route('admin.categories.store') }}" method="POST">
            @csrf
            <div
                class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800"
            >
                <label class="block text-sm">
                    <span class="text-gray-700 dark:text-gray-400">Name</span>
                    <input
                        name="name"
                        class="block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input border-red-600"
                        placeholder="Category name"
                        value="{{ old('name') }}"
                    />
                    @error('name')
                    <span class="text-xs text-red-600 dark:text-red-400">
                        {{ $message }}
                    </span>
                    @enderror
                </label>


                <label class="block mt-4 text-sm">
                    <span class="text-gray-700 dark:text-gray-400">
                      Parent category
                    </span>
                    <select
                        name="parent"
                        class="block w-full mt-1 text-sm dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 form-multiselect focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray"
                        size="10"
                    >
                        <option value="0" {{ old('parent') ?? 'selected' }}>Top level category (no parent)</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{ old('parent') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('parent')
                    <span class="text-xs text-red-600 dark:text-red-400">
                        {{ $message }}
                    </span>
                    @enderror
                </label>

                <div class="flex">

                    <div class="mr-4">
                        <img
                            class="rounded-full"
                            src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9JzMwMHB4JyB3aWR0aD0nMzAwcHgnICBmaWxsPSIjMDAwMDAwIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgOTAgOTAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDkwIDkwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cGF0aCBkPSJNMTEuNDk0LDE0LjQzOGg1OS4zMThjMi40ODgsMCw0LjQ0NywxLjk1Miw0LjQ0Nyw0LjQ0M3Y0My42ODdjMCwyLjQ4OS0xLjk1OSw0LjQ0Mi00LjQ0Nyw0LjQ0MkgxMS40OTQgIGMtMi40OSwwLTQuNDQzLTEuOTUzLTQuNDQzLTQuNDQyVjE4Ljg4MUM3LjA1MSwxNi4zOSw5LjAwNCwxNC40MzgsMTEuNDk0LDE0LjQzOHogTTExLjQ5NCwxMS4xNTFjLTQuMjU2LDAtNy43NCwzLjQ3NC03Ljc0LDcuNzMgIHY0My42ODdjMCw0LjI1NiwzLjQ4NCw3LjczNCw3Ljc0LDcuNzM0aDU5LjMxOGM0LjI1NCwwLDcuNzI5LTMuNDc5LDcuNzI5LTcuNzM0VjE4Ljg4MWMwLTQuMjU2LTMuNDc1LTcuNzMtNy43MjktNy43M0gxMS40OTR6Ij48L3BhdGg+PHBhdGggZD0iTTI4Ljk1NywyNS42NDZjMS43MDksMi40NjMsMS4xLDUuNzk2LTEuMzY5LDcuNTA1Yy0yLjQ2MywxLjcwMy01Ljc5MSwxLjA5OS03LjQ5NC0xLjM2NSAgYy0xLjcwOS0yLjQ2OC0xLjEwOS01Ljc5NSwxLjM1OS03LjUwNUMyMy45MTYsMjIuNTczLDI3LjI1NCwyMy4xODIsMjguOTU3LDI1LjY0NnogTTMxLjY2LDIzLjc3NSAgYy0yLjcxOS0zLjkyNy04LjE1Ni00LjkxMS0xMi4wNzgtMi4xOTdjLTMuOTI2LDIuNzE5LTQuOTE2LDguMTU2LTIuMTk3LDEyLjA4M2MyLjcxOSwzLjkyMiw4LjE1LDQuOTEyLDEyLjA3OCwyLjE5MyAgUzM0LjM3OSwyNy43MDMsMzEuNjYsMjMuNzc1eiI+PC9wYXRoPjxwYXRoIGQ9Ik01OS45NjksMjUuNDIyYy0wLjQyOCwwLjAzLTAuODI4LDAuMjI0LTEuMTE1LDAuNTQ3TDM1LjkxLDUwLjcwM0wyMi42NzYsNDEuMWMtMC42MTktMC40NTQtMS40NzMtMC40MTctMi4wNTEsMC4wODggIEw0Ljc3NSw1NS4wNjJjLTAuNzI5LDAuNTg0LTAuODI4LDEuNjYyLTAuMjA5LDIuMzY1YzAuNjE1LDAuNzAzLDEuNjkzLDAuNzU1LDIuMzcxLDAuMTA0bDE0Ljg2OS0xMy4wMDVsMTMuMzU0LDkuNjk3ICBjMC42NzIsMC40OTEsMS42MDUsMC4zOTYsMi4xNjgtMC4yMTRMNjEuMjcsMjguMTk4QzYyLjMwNywyNy4xMTQsNjEuNDY5LDI1LjMyMyw1OS45NjksMjUuNDIyeiI+PC9wYXRoPjxwYXRoIGQ9Ik0zNi4wMzEsNTEuMjM0Yy0wLjQyOCwwLjAzMi0wLjgyNCwwLjIyNC0xLjEwOSwwLjU0MWwtNi4yNjYsNi43NTZjLTAuNjYyLDAuNjYyLTAuNjQxLDEuNzM1LDAuMDQ3LDIuMzcgIGMwLjY4OCwwLjYzNSwxLjc2LDAuNTY3LDIuMzY5LTAuMTQxbDYuMjU2LTYuNzUxQzM4LjM2Myw1Mi45MjcsMzcuNTI1LDUxLjEzNSwzNi4wMzEsNTEuMjM0eiI+PC9wYXRoPjxwYXRoIGQ9Ik01OS45NDcsMjUuNDIyYy0xLjM5NiwwLjA4My0yLjA1NywxLjc2LTEuMDk0LDIuNzc2bDE2LjE5MywxNy40NTljMS40ODgsMS42MDksMy45LTAuNjMxLDIuNDEtMi4yMzVMNjEuMjcsMjUuOTY5ICBDNjAuOTM4LDI1LjU5NCw2MC40NDcsMjUuMzk1LDU5Ljk0NywyNS40MjJ6Ij48L3BhdGg+PHBhdGggZD0iTTc4LjU0MSwxOS41ODhjLTIuMTkzLDAtMi4xOTMsMy4yOTYsMCwzLjI5NmMyLjQ5LDAsNC40MzgsMS45NTMsNC40MzgsNC40NDJ2NDMuNjgyYzAsMi40OTUtMS45NDcsNC40NDktNC40MzgsNC40NDkgIEgxOS4yMTNjLTIuNDksMC00LjQ0My0xLjk1NC00LjQ0My00LjQ0OWMwLjA0Ny0wLjkzMi0wLjcwMy0xLjcxOC0xLjY0MS0xLjcxOHMtMS42ODgsMC43ODYtMS42NDUsMS43MTggIGMwLDQuMjU2LDMuNDczLDcuNzMsNy43MjksNy43M2g1OS4zMjhjNC4yNTYsMCw3LjcyOS0zLjQ3NCw3LjcyOS03LjczVjI3LjMyN0M4Ni4yNywyMy4wNzMsODIuNzk3LDE5LjU4OCw3OC41NDEsMTkuNTg4eiI+PC9wYXRoPjwvc3ZnPg=="
                            >
                    </div>
                    <div>
                        <label class="block mt-4 text-sm">
                            <span class="text-gray-700 dark:text-gray-400">Description</span>
                            <textarea
                                name="description"
                                class="block w-full mt-1 text-sm dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 form-textarea focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray"
                                rows="3"
                                placeholder="Enter category's description here."
                            ></textarea>
                        </label>
                    </div>
                </div>
            </div>

            <div class="flex flex-col items-center justify-end px-6 py-3 -mx-6 -mb-4 space-y-4 sm:space-y-0 sm:space-x-6 sm:flex-row bg-gray-50 dark:bg-gray-800">
                <a href="{{ route('admin.categories.index') }}" class="w-full px-5 py-3 text-sm font-medium leading-5 text-white text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg dark:text-gray-400 sm:px-4 sm:py-2 sm:w-auto active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray">
                    Cancel
                </a>
                <button type="submit" class="w-full px-5 py-3 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-lg sm:w-auto sm:px-4 sm:py-2 active:bg-blue-600 hover:bg-blue-700 focus:outline-none focus:shadow-outline-blue">
                    Create category
                </button>
            </div>
        </form>
    </div>
@endsection
